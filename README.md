# Pants

![Pants Logo](/docs/imgs/trousers.png)

Android unbuilder and rebuilder script, for modifying APK files easily.

## Install
* Install `adb`, `apktool`, `jarsigner` and `keytool`.

## Usage
1. Put `pants` into the same folder as the APK file you want to modify.
2. Run `pants` for the first time.
3. Afterwards, edit `pants` so that the disassembly and key generation lines don't run by commenting them out.
4. Make modifications to your hearts content, running `pants` each time you make a change to the disassembled files.

## FAQs
* What tool should I use to make changes?
    * Ideally just a text editor, but half the problem is knowing where to make those changes. Use something like https://github.com/skylot/jadx to get a pseudo-Java representation to figure out where you need to make changes. However, it's really hard to rebuild using JADX directly.
